/*! Common traits for the config module.
*/

/// The Example trait is used for configuration structs
/// to provide an example configuration.
pub trait Example {
    /// The implementation should provide an example configuration
    /// using various required and optional configurations.
    fn example() -> Self;
}
