/*! Read configuration from a file
 */

use std::fs;

use crate::config::main::{configure, Config, ConfigError};

/// Read configuration from a file path.
pub fn read_config_file(path: &std::path::PathBuf) -> Result<Config, ConfigError> {
    debug!(
        "reading config file from {}",
        path.as_os_str().to_str().unwrap()
    );
    let read_result = fs::read_to_string(path);
    let contents = match read_result {
        Ok(data) => data,
        Err(err) => {
            error!("failed to read config file ('{:?}'): {}", path, err);
            return Err(ConfigError::IOError);
        }
    };

    let config = configure(&contents)?;
    debug!("parsed config: {:?}", config);
    Ok(config)
}
