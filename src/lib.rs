/*! All the useful stuff of Helz is documented here.
See especially the "modules" section of this crate.
*/

#[macro_use]
extern crate log;

#[deny(missing_docs)]
pub mod config;
#[deny(missing_docs)]
pub mod notifying;
#[deny(missing_docs)]
pub mod polling;
#[deny(missing_docs)]
pub mod types;

pub use crate::{config::file::read_config_file, polling::config::Polling};
