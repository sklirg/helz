/** Configuration File Watching
 */
use libhelz::config::file::read_config_file;
use libhelz::config::main::Config;
use notify::{RecursiveMode, Watcher};

#[cfg(target_os = "macos")]
pub type NotifyWatcher = notify::fsevent::FsEventWatcher;
#[cfg(target_os = "linux")]
pub type NotifyWatcher = notify::inotify::INotifyWatcher;

pub fn watcher(
    path: &std::path::Path,
    tx: tokio::sync::watch::Sender<Option<Config>>,
) -> Result<NotifyWatcher, notify::Error> {
    info!("Watching Helz configuration for changes");

    let p = path.to_owned();

    // don't drop this
    let mut watcher: NotifyWatcher =
        notify::recommended_watcher(move |res| watched_file_changed_handler(res, &tx, &p))?;

    watcher.watch(path, RecursiveMode::NonRecursive)?;

    Ok(watcher)
}

fn watched_file_changed_handler(
    res: notify::Result<notify::Event>,
    tx: &tokio::sync::watch::Sender<Option<Config>>,
    path: &std::path::Path,
) {
    match res {
        Ok(event) => match event.kind {
            notify::event::EventKind::Modify(_) |
            // only care about the Create-part of it,
            // as we don't want to re-read a deleted file.
            notify::event::EventKind::Create(_) => {
                match read_config_file(&path.to_path_buf()) {
                    Ok(config) => match tx.send(Some(config)) {
                            Ok(_) => debug!("Triggering config update"),
                            Err(err) => error!("Failed to inform Helz about configuration change: {}", err),
                        },
                        Err(err) => error!("Received file watching error: {:?}", err),
                    };
            },
            // We don't care about any other events.
            _ => {}
        },
        Err(err) => error!("Failed to listen for file changes: {}", err),
    }
}
