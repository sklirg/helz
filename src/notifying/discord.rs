/*! Discord notifier
*/

use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use serde_json::json;
use serenity::http::Http;

use crate::config::traits::Example;
use crate::notifying::traits::{error_message_detailed, ok_message, Notifier};
use crate::polling::config::HelzCheckStatus;
use crate::types::string_secret::Secret;

/// Configuration for the Discord notifier
///
/// The values can be configured in TOML as follows:
/// ```
/// # use libhelz::notifying::discord::DiscordNotifierConfig;
/// # let conf: DiscordNotifierConfig = toml::from_str(r#"
/// webhook_id = 1234
/// api_token = "api token"
/// # "#).expect("example configuration should work");
/// ```
///
/// Full example:
/// ```
/// # use libhelz::config::main::configure;
/// # let config = configure(r#"
/// [[polling]]
/// [polling.notifier]
/// interval = "5s"
/// [polling.notifier.discord]
/// webhook_id = 1234
/// api_token = "api token"
/// # "#)
/// # .expect("example configuration should work");
/// ```
#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Eq)]
pub struct DiscordNotifierConfig {
    /// The webhook ID for a Discord webhook.
    /// If you copy the webhook from the discord settings page,
    /// this is the numerical value of the URL, after `/webhooks/<webhook_id>`.
    pub webhook_id: u64,
    /// The api token is the last part of the webhook URL,
    /// `/webhooks/<webhook_id>/<api_token>`.
    pub api_token: Secret,
    /// bot_name is optional and can be set to override the name
    /// set in the Discord webhook.
    pub bot_name: Option<String>,
}

/// The internal struct for a Discord notifier
#[derive(Clone)]
pub struct DiscordNotifier {
    webhook_id: u64,
    api_token: String,
    bot_name: Option<String>,
}

impl From<DiscordNotifierConfig> for DiscordNotifier {
    fn from(conf: DiscordNotifierConfig) -> Self {
        DiscordNotifier {
            webhook_id: conf.webhook_id,
            api_token: conf.api_token.expose(),
            bot_name: conf.bot_name,
        }
    }
}

#[async_trait]
impl Notifier for DiscordNotifier {
    async fn notify(&self, errors: &[HelzCheckStatus], whoami: &str) {
        trace!("discord notify to webhook {}", self.webhook_id);

        let body = error_message_detailed(errors, whoami);

        let webhook_id_ = self.webhook_id.to_owned();
        let api_token_ = self.api_token.to_owned();

        trace!("discord task");
        // ToDo: create this during init and reuse it
        let client = Http::new(&self.api_token);

        let mut content = json!({ "content": body })
            .as_object()
            .expect("failed to convert discord notification body to json")
            .to_owned();

        if let Some(bot_name) = &self.bot_name {
            content.insert(
                "username".to_owned(),
                serde_json::Value::String(bot_name.to_owned()),
            );
        }

        let webhook_id = webhook_id_;
        let api_token = api_token_;

        match client
            .execute_webhook(webhook_id, &api_token, false, &content)
            .await
        {
            Ok(_) => trace!("successful discord webhook post"),
            Err(err) => error!("failed to post discord hook: {}", err),
        };
    }

    async fn ok(&self, errors: &[HelzCheckStatus], whoami: &str) {
        trace!("discord notify ok to webhook {}", self.webhook_id);

        let body = ok_message(errors, whoami);

        let webhook_id_ = self.webhook_id.to_owned();
        let api_token_ = self.api_token.to_owned();

        trace!("discord async time");

        // ToDo: create this during init and reuse it
        let client = Http::new(&self.api_token);

        let mut message = json!({ "content": body })
            .as_object()
            .expect("failed to convert discord notification body to json")
            .to_owned();

        if let Some(bot_name) = &self.bot_name {
            message.insert(
                "username".to_owned(),
                serde_json::Value::String(bot_name.to_owned()),
            );
        }

        let webhook_id = webhook_id_;
        let api_token = api_token_;

        trace!("spawned discord task");
        match client
            .execute_webhook(webhook_id, &api_token, false, &message)
            .await
        {
            Ok(_) => trace!("successful discord webhook post"),
            Err(err) => error!("failed to post discord hook: {}", err),
        };
    }
}

impl Example for DiscordNotifierConfig {
    fn example() -> Self {
        Self {
            webhook_id: 0,
            api_token: Secret("foo".to_string()),
            bot_name: None,
            // bot_name: Some("foo-bot"),
        }
    }
}
