/*! Stdout notifier

Outputs to standard output through the default print function.
Useful for debugging and if you ship your log output off to somewhere else anyways.
*/

use async_trait::async_trait;
use serde::{Deserialize, Serialize};

use crate::config::traits::Example;
use crate::notifying::traits::Notifier;
use crate::polling::config::{HelzCheckError, HelzCheckStatus};

/// Configuration for the stdout notifier
///
/// The values can be configured in TOML as follows:
/// ```
/// # use libhelz::notifying::stdout::StdoutNotifierConfig;
/// # let conf: StdoutNotifierConfig = toml::from_str(r#"
/// prefix = "out of this world: "
/// # "#).expect("example configuration should work");
/// ```
///
/// Full example:
/// ```
/// # use libhelz::config::main::configure;
/// # let config = configure(r#"
/// [[polling]]
/// [polling.notifier]
/// interval = "5s"
/// [polling.notifier.stdout]
/// prefix = "out of this world: "
/// # "#).expect("example configuration should work");
/// ```
#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Eq)]
pub struct StdoutNotifierConfig {
    /// A prefix for the messages printed to stdout.
    pub prefix: String,
}

#[derive(Clone)]
/// The runtime struct of a stdout notifier.
pub struct StdoutNotifier {
    prefix: String,
}

impl From<StdoutNotifierConfig> for StdoutNotifier {
    fn from(conf: StdoutNotifierConfig) -> Self {
        StdoutNotifier {
            prefix: conf.prefix,
        }
    }
}

impl From<StdoutNotifierConfig> for crate::notifying::config::HelzNotificationConfig {
    fn from(conf: StdoutNotifierConfig) -> Self {
        let notifier = conf;

        #[allow(deprecated)]
        Self {
            discord: None,
            interval: None,
            interval_s: None,
            opsgenie: None,
            slack: None,
            stdout: Some(notifier),
            timeout: None,
            timeout_s: None,
            webex: None,
        }
    }
}

#[async_trait]
impl Notifier for StdoutNotifier {
    async fn notify(&self, errors: &[HelzCheckStatus], whoami: &str) {
        let first_error = &errors[0];
        let last_error = &errors[errors.len() - 1];

        let mut unique_errors_types: Vec<HelzCheckError> = Vec::new();
        let mut unique_errors: Vec<HelzCheckStatus> = Vec::new();

        for err in errors {
            debug!("errorx:{:?}", err.response);

            if !unique_errors_types.contains(&err.response) {
                unique_errors_types.push(err.response.to_owned());
                unique_errors.push(err.to_owned());
            }
        }

        println!(
            "{}{}{} {}; {} events in {}s",
            self.prefix,
            whoami,
            first_error.name,
            last_error.response,
            &errors.len(),
            first_error.time.elapsed().as_secs()
        );
    }

    async fn ok(&self, errors: &[HelzCheckStatus], whoami: &str) {
        let first_error = &errors[0];
        println!(
            "{}{}{} is back up after {}s and {} errors",
            self.prefix,
            whoami,
            first_error.name,
            first_error.time.elapsed().as_secs(),
            &errors.len()
        );
    }
}

impl Example for StdoutNotifierConfig {
    fn example() -> Self {
        Self {
            prefix: "".to_owned(),
        }
    }
}
