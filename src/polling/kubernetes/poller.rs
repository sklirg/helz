/*! Kubernetes poller
 */
use async_trait::async_trait;
use k8s_openapi::api::apps::v1::Deployment;
use kube::{Api, Client};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::convert::From;
use std::sync::{Arc, Mutex};

use crate::polling::config::{HelzCheck, HelzCheckError, HelzCheckResponse, PollerUpdateChannel};

use crate::polling::kubernetes::config::*;
use crate::polling::kubernetes::types::*;

/// Runtime struct for the Kubernetes poller.
pub struct Kubernetes {
    namespace: Scope,
    deployment: Option<String>,
    auto: bool,
    annotation: String,
    client: Option<Client>,
    configuration_channel: Option<PollerUpdateChannel>,
    tracked_objects: Arc<Mutex<HashMap<KubernetesResourceKey, HelzKubernetesResource>>>,
    tracked_objects_sender: TrackedObjectsSender,
    tracked_resource_gvks: Option<Vec<TrackedResourceWithStatus>>,
}

impl From<KubernetesConfig> for Kubernetes {
    fn from(conf: KubernetesConfig) -> Self {
        if (!conf.auto.unwrap_or(false)) && conf.deployment.is_none() {
            panic!("Kubernetes poller configuration requires either 'auto=true' or 'deployment' to be set.");
        }

        let client = match conf.client_config {
            Some(client_conf) => {
                let uri = http::uri::Uri::from(client_conf.cluster_url);
                let mut conf = kube::Config::new(uri);

                // TODO: implement this
                // conf.root_cert = client_conf.root_cert;

                // TODO: Separate these in config.
                conf.connect_timeout = client_conf.timeout;
                conf.read_timeout = client_conf.timeout;
                conf.accept_invalid_certs = client_conf.accept_invalid_certs.unwrap_or(false);

                let builder = kube::client::ClientBuilder::try_from(conf)
                    .expect("failed to create kubernetes config from URL");
                Some(builder.build())
            }
            None => None,
        };

        let annotation = conf
            .annotation
            .unwrap_or_else(|| DEFAULT_ANNOTATION.to_string());

        let (objects_update_tx, _) = tokio::sync::mpsc::unbounded_channel::<TrackedObject>();

        Self {
            client,
            annotation,
            namespace: conf.namespace,
            deployment: conf.deployment,
            auto: conf.auto.unwrap_or_default(),
            configuration_channel: None,
            tracked_objects: Arc::new(Mutex::new(HashMap::new())),
            tracked_objects_sender: objects_update_tx,
            tracked_resource_gvks: conf.resources,
        }
    }
}

#[async_trait]
impl HelzCheck for Kubernetes {
    async fn configure(
        &mut self,
        channel: crate::polling::config::PollerUpdateChannel,
    ) -> Result<(), HelzCheckError> {
        let client = match Client::try_default().await {
            Ok(c) => c,
            Err(e) => {
                return Err(HelzCheckError::KubernetesError(format!(
                    "failed to configure kubernetes client: {e}"
                )))
            }
        };

        self.client = Some(client.to_owned());
        self.configuration_channel = Some(channel);

        match client.apiserver_version().await {
            Ok(v) => trace!("Kubernetes client connected to cluster version {:?}", v),
            Err(e) => error!("Failed to get kubernetes cluster version: {}", e),
        };

        let (objects_update_tx, objects_update_rx) =
            tokio::sync::mpsc::unbounded_channel::<TrackedObject>();

        self.tracked_objects_sender = objects_update_tx;

        self.start_configuration_tracker(objects_update_rx).await;

        Ok(())
    }

    #[tracing::instrument(skip(self))]
    async fn run(&self) -> Result<HelzCheckResponse, HelzCheckError> {
        debug!("k8s check");
        let client = if let Some(client) = &self.client {
            client
        } else {
            return Err(HelzCheckError::KubernetesError(
                "no working client configured".to_string(),
            ));
        };

        if self.auto {
            self.find_resources().await;
            return self.helz_check_tracked_objects().await;
        } else {
            let namespace = match &self.namespace {
                Scope::Namespaced(ns) => ns,
                Scope::Cluster(_) => return Err(HelzCheckError::ConfigurationError("Kubernetes poller is configured in Cluster mode for Deployment monitoring. Select a namespace, please.".to_owned())),
            };
            // backwards compatible if not setting "auto"
            let client: Api<Deployment> = Api::namespaced(client.to_owned(), namespace);
            return self
                .kube_deployment(
                    client,
                    &self
                        .deployment
                        .to_owned()
                        .expect("deployment name must be defined"),
                )
                .await;
        }
    }
}

impl Kubernetes {
    async fn helz_check_tracked_objects(&self) -> Result<HelzCheckResponse, HelzCheckError> {
        // Clone a local copy of self.tracked_objects before starting this polling run.
        // We use it later together with seen_this_round to remove objects that have
        // been removed from Kubernetes state.
        let tracked_objects: HashMap<KubernetesResourceKey, HelzKubernetesResource>;
        {
            let tracked_objects_ref = match self.tracked_objects.lock() {
                Ok(guard) => guard,
                Err(e) => {
                    return Err(HelzCheckError::KubernetesError(format!(
                        "failed to lock lookup table for status checking..: {e}"
                    )))
                }
            };
            tracked_objects = tracked_objects_ref.to_owned();
        }

        let mut errors: Vec<HelzCheckError> = Vec::new();
        for (_, obj) in tracked_objects {
            let api = create_api_client(self.client.clone().unwrap(), &obj);
            let item = match api.get_status(&obj.name).await {
                Ok(obj) => obj,
                Err(e) => {
                    match &e {
                        kube::Error::Api(api_error) => {
                            if api_error.code == 404 {
                                trace!("Resource we tracked is gone, stopping polling of it (upstream error: {api_error}).");
                            } else {
                                warn!("API returned an error: {api_error}");
                            }
                        }
                        _ => warn!("API request returned an error: {e}"),
                    }
                    match self
                        .tracked_objects_sender
                        .send(TrackedObject::Remove(obj.key()))
                    {
                        Ok(_) => (),
                        Err(se) => {
                            warn!("Failed to remove tracked object after API error: {se} ({e})")
                        }
                    };
                    continue;
                }
            };
            match item.metadata.name {
                Some(name) => name,
                None => continue,
            };

            let status = self.get_status(item.data);

            if let Err(e) = status {
                errors.push(e);
            }
        }

        if errors.is_empty() {
            Ok(HelzCheckResponse { success: true })
        } else {
            Err(HelzCheckError::MultipleErrors(errors))
        }
    }

    fn consider_deployment(&self, deploy: Deployment) -> Result<HelzCheckResponse, HelzCheckError> {
        let conditions = &deploy
            .status
            .as_ref()
            .ok_or(HelzCheckError::KubernetesStatusError)?
            .conditions;

        if let Some(conditions) = conditions {
            let bad_conditions: Vec<&k8s_openapi::api::apps::v1::DeploymentCondition> = conditions
                .iter()
                .filter(|condition| condition.status == "False" || condition.status == "Unknown")
                .collect();

            if !bad_conditions.is_empty() {
                debug!("bad conditions found: {:?}", bad_conditions);

                let error_messages = bad_conditions
                    .iter()
                    .map(|condition| {
                        if let Some(message) = &condition.message {
                            message.to_owned()
                        } else {
                            "unknown error".to_string()
                        }
                    })
                    .collect::<Vec<String>>()
                    .join(",");

                return Err(HelzCheckError::KubernetesError(format!(
                    "{}/deployment/{}: {}",
                    self.namespace,
                    self.deployment.to_owned().unwrap(),
                    error_messages
                )));
            }
        }

        Ok(HelzCheckResponse { success: true })
    }

    async fn start_configuration_tracker(
        &self,
        objects_update_rx: tokio::sync::mpsc::UnboundedReceiver<TrackedObject>,
    ) {
        let tracked_objects_handle = self.tracked_objects.clone();

        // Start tracker watcher
        let _object_tracker = tokio::spawn(async move {
            // TODO: Move main polling logic for adding/removing to core loop here.
            let tracked_objects_copy = tracked_objects_handle.to_owned();

            let mut receiver = objects_update_rx;
            while let Some(update) = receiver.recv().await {
                debug!("Got K8s update: {update}");
                match update {
                    TrackedObject::AddDeployment(resource) => {
                        let mut tracked_objects = match tracked_objects_copy.lock() {
                            Ok(guard) => guard,
                            Err(e) => {
                                error!("failed to aquire mutex lock: {e}");
                                return;
                            }
                        };
                        tracked_objects.insert(resource.to_owned().key(), resource.to_owned());
                    }
                    TrackedObject::RemoveDeployment(key) | TrackedObject::Remove(key) => {
                        let mut tracked_objects = match tracked_objects_copy.lock() {
                            Ok(guard) => guard,
                            Err(e) => {
                                error!("failed to aquire mutex lock: {e}");
                                return;
                            }
                        };

                        tracked_objects.remove(&key);
                    }
                    TrackedObject::Add(resource) => {
                        let mut tracked_objects = match tracked_objects_copy.lock() {
                            Ok(guard) => guard,
                            Err(e) => {
                                error!("failed to aquire mutex lock: {e}");
                                return;
                            }
                        };
                        tracked_objects.insert(resource.key(), resource.to_owned());
                    }
                }
            }
        });
    }

    async fn kube_deployment(
        &self,
        client: Api<Deployment>,
        deployment_name: &str,
    ) -> Result<HelzCheckResponse, HelzCheckError> {
        let deploy = match client.get_status(deployment_name).await {
            Ok(d) => d,
            Err(e) => {
                error!("kubernetes deployment.get_status error: {}", e);
                return Err(HelzCheckError::KubernetesStatusError);
            }
        };

        self.consider_deployment(deploy)
    }

    async fn find_resources(&self) {
        debug!("Finding resources to track");

        let client = self.client.clone().expect("Kubernetes client should work");
        let discovery = kube::Discovery::new(client.clone())
            .run()
            .await
            .expect("discovery client should work");

        if self
            .tracked_resource_gvks
            .clone()
            .unwrap_or_default()
            .is_empty()
        {
            warn!("No GVKs configured, no polling to do.");
            return;
        }
        let tracked_resource_gvks_ = self.tracked_resource_gvks.clone().unwrap_or_default();
        let tracked_resource_gvks = tracked_resource_gvks_
            .iter()
            .map(|e| e.gvk.as_str())
            .collect::<Vec<&str>>();
        trace!(
            "Resources considered for tracking: {:?}",
            tracked_resource_gvks
        );

        for group in discovery.filter(&tracked_resource_gvks).groups() {
            for (ar, caps) in group.recommended_resources() {
                let gvk = format!("{}/{}/{}", ar.group, ar.version, ar.kind);
                if !tracked_resource_gvks.contains(&gvk.as_str()) {
                    trace!(
                        "Skipping resouce '{gvk}' in '{}' as not configured",
                        group.name()
                    );
                    continue;
                }
                if !caps.supports_operation(kube::discovery::verbs::LIST) {
                    trace!(
                        "Skipping resouce '{gvk}' in '{}' as it doesn't support listing",
                        group.name()
                    );
                    continue;
                }
                trace!(
                    "Tracking resource {gvk} ({}) in {:?} scope",
                    group.name(),
                    caps.scope
                );

                let api: Api<kube::api::DynamicObject> =
                    if caps.scope == kube::discovery::Scope::Cluster {
                        Api::all_with(client.clone(), &ar)
                    } else {
                        Api::namespaced_with(client.clone(), &self.namespace.to_string(), &ar)
                    };

                // Finding resources with status field
                let items = match api.list(&Default::default()).await {
                    Ok(items) => items,
                    Err(e) => {
                        warn!("API Error: {e}");
                        continue;
                    }
                };
                for item in items {
                    let name = match item.clone().metadata.name {
                        Some(name) => name,
                        None => continue,
                    };

                    // Skip items without the Helz watch annotation.
                    if !self.enabled(&item) {
                        trace!(
                            "Skipping {} because it doesn't have {}=enabled",
                            item.metadata
                                .name
                                .unwrap_or_else(|| "<unnamed resource>".to_owned()),
                            &self.annotation,
                        );
                        continue;
                    }

                    match api.get_status(&name).await {
                        Ok(obj) => obj,
                        Err(_) => continue,
                    };

                    let status = self.get_status(item.data);

                    if status.is_err() {
                        debug!(
                            "{}/{} has status: {:?}",
                            item.metadata
                                .namespace
                                .unwrap_or_else(|| "<NoNS>".to_owned()),
                            name,
                            status
                        );
                    }

                    let r = HelzKubernetesResource::new(
                        ar.group.to_owned(),
                        ar.version.to_owned(),
                        ar.kind.to_owned(),
                        self.namespace.clone(),
                        name,
                    );
                    match self
                        .tracked_objects_sender
                        .clone()
                        .send(TrackedObject::Add(r))
                    {
                        Ok(_) => (),
                        Err(e) => warn!("failed to send object tracking query {e}"),
                    }
                }
            }
        }

        let tracked_objects: HashMap<KubernetesResourceKey, HelzKubernetesResource>;
        {
            let tracked_objects_ref = match self.tracked_objects.lock() {
                Ok(guard) => guard,
                Err(e) => {
                    warn!("Failed to lock Helz Kubernetes table for object lookups. If this happens often, there might be high contention on the table/many resources tracked. ({e})");
                    return;
                }
            };
            tracked_objects = tracked_objects_ref.to_owned();
        }

        // Checking for objects to remove from tracking.
        for (key, obj) in tracked_objects {
            let api = create_api_client(self.client.clone().unwrap(), &obj);
            let item = match api.get_status(&obj.name).await {
                Ok(o) => o,
                Err(e) => {
                    match &e {
                        kube::Error::Api(api_error) => {
                            if api_error.code == 404 {
                                trace!("Resource we tracked is gone, stopping polling of it (upstream error: {api_error}).");
                            } else {
                                warn!("API returned an error: {api_error}");
                            }
                        }
                        _ => warn!("API request returned an error: {e}"),
                    }
                    continue;
                }
            };
            if !self.enabled(&item) {
                match self.tracked_objects_sender.send(TrackedObject::Remove(key)) {
                    Ok(_) => (),
                    Err(e) => warn!("Failed to send object tracking update: {e}"),
                }
            }
        }
    }

    fn get_status(&self, json: serde_json::Value) -> Result<HelzCheckResponse, HelzCheckError> {
        let status = json
            .get("status")
            .expect("Expecting get_status to only give us objects with status");

        let status_conditions = match status.get("conditions") {
            Some(conditions) => match conditions.as_array() {
                Some(arr) => arr,
                None => {
                    return Err(HelzCheckError::KubernetesError(
                        "Failed to cast 'conditions' to an array".to_owned(),
                    ))
                }
            },
            None => {
                return Err(HelzCheckError::KubernetesError(
                    "JSON data did not contain a 'conditions' map".to_owned(),
                ))
            }
        };

        let mut checks = self.tracked_resource_gvks.clone().unwrap();
        for condition in status_conditions {
            match serde_json::from_value(condition.clone()) {
                Ok(condition) => check_condition_matches(&mut checks, &condition),
                Err(_) => false,
            };
        }

        if checks.is_empty() {
            Ok(HelzCheckResponse { success: true })
        } else {
            let mut msg = "".to_owned();
            for check in checks {
                let this_check_reason: Vec<String> = check
                    .conditions
                    .into_iter()
                    .map(|c| {
                        format!(
                            "{}: {}",
                            c.type_,
                            c.reason
                                .unwrap_or_else(|| format!("not [{}]", c.status.join(",")))
                        )
                    })
                    .collect();
                msg += &this_check_reason.join(",");
            }
            Err(HelzCheckError::KubernetesError(msg))
        }
    }

    /// Check an object for the Helz enabled annotation
    fn enabled(&self, obj: &kube::core::DynamicObject) -> bool {
        match obj
            .metadata
            .annotations
            .clone()
            .unwrap_or_default()
            .get(&self.annotation)
        {
            Some(value) => value == "enabled",
            None => false,
        }
    }
}

#[derive(Deserialize, Serialize)]
struct SimpleCondition {
    #[serde(rename = "type")]
    type_: String,
    status: String,
    reason: Option<String>,
}

fn check_condition_matches(
    checks: &mut Vec<TrackedResourceWithStatus>,
    condition: &SimpleCondition,
) -> bool {
    for (i, check) in &mut checks.iter_mut().enumerate() {
        for check_condition in &mut check.conditions {
            if check_condition.type_ == condition.type_
                && check_condition.status.contains(&condition.status)
            {
                checks.remove(i);
                return true;
            } else if check_condition.type_ == condition.type_ {
                // If we didn't find a matching status for the type,
                // assign reason to the check so we can show that to the user.
                check_condition.reason = condition.reason.to_owned();
            }
        }
    }

    false
}

fn create_api_client(
    client: kube::Client,
    obj: &HelzKubernetesResource,
) -> Api<kube::api::DynamicObject> {
    let gvk = kube::core::GroupVersionKind::gvk(&obj.group, &obj.version, &obj.kind);
    let ar = kube::api::ApiResource::from_gvk(&gvk);
    match &obj.namespace {
        Scope::Cluster(_) => Api::all_with(client, &ar),
        Scope::Namespaced(namespace) => Api::namespaced_with(client, namespace, &ar),
    }
}
