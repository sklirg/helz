/*! The overall program config of all pollers.
 */

use async_trait::async_trait;
use chrono::prelude::*;
use serde::{Deserialize, Serialize};
use std::convert::From;
use std::fmt;
use std::time;

use crate::config::traits::Example;
use crate::notifying::config::{HelzNotification, HelzNotificationConfig};
use crate::polling::http::{HttpPolling, HttpPollingConfig};
use crate::polling::kubernetes::{Kubernetes, KubernetesConfig};
use crate::polling::systemd::{Systemd, SystemdConfig};
use crate::polling::tcp::TcpPollingConfig;

#[derive(Clone, Deserialize, Debug, Serialize, Eq, PartialEq)]
/// The overarching configuration of a Helz instance.
pub struct PollingConfig {
    /// A name for this polling configuration. Will be generated based
    /// on configured pollers if left empty.
    pub name: Option<String>,
    /// Name of systemd unit to query if require_systemd_enabled
    /// is set to true.
    pub systemd_unit: Option<String>,
    /// Require a systemd unit to be 'enabled' to run health
    /// checks.
    pub require_systemd_enabled: Option<bool>,
    /// Interval in seconds for how often to execute the pollers.
    #[deprecated]
    pub interval_s: Option<u64>,
    /// Interval for how often to execute the pollers.
    #[serde(default)]
    #[serde(with = "humantime_serde")]
    pub interval: Option<time::Duration>,
    // All structs have to be defined **after** single fields,
    // otherwise toml will have trouble serializing the struct
    // into a TOML string.
    /// Configuration for a Kubernetes poller.
    pub kubernetes: Option<KubernetesConfig>,
    /// Configuration for a TCP poller.
    pub tcp: Option<TcpPollingConfig>,
    /// Configuration for a systemd poller.
    pub systemd: Option<SystemdConfig>,
    /// Configuration for a HTTP poller.
    pub http: Option<HttpPollingConfig>,
    /// Configuration for notifications for this poller.
    pub notifier: Option<HelzNotificationConfig>,
}

/// Polling contains the runtime values parsed from the supplied
/// [PollingConfig](crate::polling::config::PollingConfig).
pub struct Polling {
    /// The name of this poller.
    pub name: String,
    /// Systemd unit to check for being enabled.
    /// Requires `require_systemd_enabled` to be set.
    pub systemd_unit: Option<String>,
    /// Skip health checking if the systemd unit is `disabled`.
    pub require_systemd_enabled: bool,
    /// How often to poll.
    pub interval: time::Duration,
    /// A list of pollers to execute.
    pub pollers: Vec<Box<dyn HelzCheck + Send + Sync>>,
    /// A set of notifiers to send notifications through.
    pub notifier: Option<HelzNotification>,
}

impl From<PollingConfig> for Polling {
    fn from(conf: PollingConfig) -> Polling {
        let mut generated_name = "".to_owned();
        let notifier = conf.notifier.map(HelzNotification::from);

        let mut pollers: Vec<Box<dyn HelzCheck + Send + Sync>> = Vec::new();
        if let Some(tcp) = conf.tcp {
            debug!("adding tcp poller");
            generated_name = format!("tcp={}:{}", tcp.address, tcp.port);
            let t = Box::new(tcp) as Box<dyn HelzCheck + Send + Sync>;
            pollers.push(t);
        }
        if let Some(systemd_conf) = conf.systemd {
            debug!("adding systemd poller");
            generated_name = format!("{},systemd={}", generated_name, systemd_conf.unit);
            let systemd = Systemd::from(systemd_conf);
            pollers.push(Box::new(systemd) as Box<dyn HelzCheck + Send + Sync>);
        }
        if let Some(http_conf) = conf.http {
            debug!("adding http poller");
            generated_name = format!("{},http={}", generated_name, http_conf.url);
            let http = HttpPolling::from(http_conf);
            pollers.push(Box::new(http) as Box<dyn HelzCheck + Send + Sync>);
        }
        if let Some(conf) = conf.kubernetes {
            debug!("adding kubernetes poller");
            let mut name = String::new();
            if let Some(deploy) = conf.deployment.to_owned() {
                name = format!("{name} {deploy}");
            }
            if conf.auto.unwrap_or(false) {
                name = format!("{name} (auto)");
            }
            generated_name = format!("{},k8s={}:{}", generated_name, conf.namespace, name);
            let http = Kubernetes::from(conf);
            pollers.push(Box::new(http) as Box<dyn HelzCheck + Send + Sync>);
        }

        let name = match conf.name {
            Some(name) => name,
            None => generated_name,
        };

        // TODO: require systemd_unit to be defined if require_systemd_unit is true

        let mut interval = conf
            .interval
            .unwrap_or_else(|| time::Duration::from_secs(5));

        // Keep backwards compatability with 'interval_s'.
        #[allow(deprecated)]
        if let Some(interval_s) = conf.interval_s {
            warn!("The configuration parameter 'interval_s' has been deprecated. Use 'interval' instead.");
            if conf.interval.is_some() {
                warn!("Both 'interval' and 'interval_s' are configured. 'interval' will take precedence.");
            } else {
                interval = time::Duration::from_secs(interval_s);
            }
        }
        Polling {
            name,
            systemd_unit: conf.systemd_unit,
            require_systemd_enabled: conf.require_systemd_enabled.unwrap_or(false),
            interval,
            pollers,
            notifier,
        }
    }
}

impl Example for PollingConfig {
    fn example() -> Self {
        #[allow(deprecated)]
        PollingConfig {
            name: Some("HelzCheck".to_owned()),
            systemd_unit: None,
            require_systemd_enabled: None,
            interval_s: None,
            interval: Some(time::Duration::from_secs(5)),
            http: None,
            kubernetes: Some(KubernetesConfig::example()),
            tcp: Some(TcpPollingConfig::example()),
            systemd: Some(SystemdConfig::example()),
            notifier: Some(HelzNotificationConfig::example()),
        }
    }
}

#[derive(Debug)]
/// Contains the response for a successful health check.
pub struct HelzCheckResponse {
    /// Contains whether the response was successful or not.
    /// Not really used in practice, is always returned as `true`.
    pub success: bool,
}

#[derive(Clone, Debug, PartialEq, Eq)]
/// Contains the variants of a health check failure.
pub enum HelzCheckError {
    /// Error during poller configuration
    ConfigurationError(String),
    /// Kubernetes health check error.
    KubernetesError(String),
    /// Occurs if there was a problem reading the status response from an API request.
    KubernetesStatusError,
    /// Error in a HTTP request.
    HttpRequestError(String),
    /// Non-expected status code from a HTTP request.
    HttpRequestStatusCodeError(String),
    /// Response was not JSON even though a JSON check was defined.
    HttpNotJson(String),
    /// Response contained invalid JSON data.
    HttpJsonInvalidData(String),
    /// Error for a TCP health check.
    TcpPollingFailure(String),
    /// Error for a systemd health check.
    SystemdFailure(String),
    /// Multiple `HelzCheckError`s.
    MultipleErrors(Vec<HelzCheckError>),
}

impl fmt::Display for HelzCheckError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            HelzCheckError::ConfigurationError(msg) => write!(f, "{msg}"),
            HelzCheckError::KubernetesError(msg) => write!(f, "{msg}"),
            HelzCheckError::KubernetesStatusError => write!(f, "failed to read kubernetes status"),
            HelzCheckError::HttpRequestError(msg) => write!(f, "{msg}"),
            HelzCheckError::HttpRequestStatusCodeError(msg) => write!(f, "{msg}"),
            HelzCheckError::TcpPollingFailure(msg) => write!(f, "{msg}"),
            HelzCheckError::SystemdFailure(msg) => write!(f, "{msg}"),
            HelzCheckError::HttpNotJson(msg) => write!(f, "{msg}"),
            HelzCheckError::HttpJsonInvalidData(msg) => write!(f, "{msg}"),
            HelzCheckError::MultipleErrors(errors) => {
                let msg: Vec<String> = errors.iter().map(|e| e.to_string()).collect();
                write!(f, "Multiple Errors: [{}]", msg.join(","))
            }
        }
    }
}

#[derive(Clone)]
/// The wrapper struct for a health check instance
pub struct HelzCheckStatus {
    /// Name of the health check
    pub name: String,
    /// Error response of the check
    pub response: HelzCheckError,
    /// At what time the check occured.
    pub time: time::Instant,
    /// Human-readable time of the check.
    pub human_time: chrono::DateTime<Local>,
}

#[async_trait]
/// Trait for implementing Health Checks.
pub trait HelzCheck: Send {
    /// The health check is spawned in an async context during application
    /// boot and runs forever on the specified interval. This check should
    /// return the health check response for every request, both for successes
    /// and for errors. The notifier configuration decides when and if to notify.
    async fn run(&self) -> Result<HelzCheckResponse, HelzCheckError>;

    /// configure() can be used to further configure something required
    /// which can not be statically loaded, i.e. loading
    /// things from a remote system or other operations
    /// that are run async.
    /// Note that if this is required, Helz wil start
    /// the poller, but the poller might indefinitely error
    /// if the configuration function failed.
    async fn configure(&mut self, _channel: PollerUpdateChannel) -> Result<(), HelzCheckError> {
        Ok(())
    }
}

/// Enum used to inform about configuration updates.
pub enum PollerUpdate {
    /// Add a HelzCheck from Trait
    Add(Polling),
    /// Remove a HelzCheck from Trait
    Remove(Polling),
    /// Adds a Poller from Configuration
    Configure(PollingConfig),
    /// Removes a Poller based on Configuration
    Deconfigure(PollingConfig),
}

/// Transmit channel for sending configuration updates to the main process.
pub type PollerUpdateChannel = tokio::sync::mpsc::UnboundedSender<PollerUpdate>;

impl Polling {
    /// This configures pollers in case they have extra
    /// configuration to be done. It will call
    /// [configure()](crate::polling::config::HelzCheck::configure)
    /// on each defined [HelzCheck](crate::polling::config::HelzCheck).
    #[tracing::instrument(skip(self))]
    pub async fn configure_pollers(
        &mut self,
        poller_update_tx: PollerUpdateChannel,
    ) -> Result<(), HelzCheckError> {
        for poller in &mut self.pollers {
            poller.configure(poller_update_tx.to_owned()).await?
        }

        Ok(())
    }

    /// This spawns an infinitely running loop for each poller in the program.
    /// It also handles notifying based on the specified notifier configuration.
    #[tracing::instrument(skip(self))]
    pub async fn start(mut self: Polling, whoami: String, _poller_update_tx: PollerUpdateChannel) {
        trace!("starting poller {}", self.name);

        let mut ticker = tokio::time::interval(self.interval);
        let mut errors: Vec<HelzCheckStatus> = Vec::new();

        loop {
            trace!("scheduling task");
            ticker.tick().await;

            match self.poll(_poller_update_tx.to_owned()).await {
                Some(errs) => {
                    errors.extend(errs);
                }
                None => {
                    if !errors.is_empty() {
                        debug!("back to ok");

                        if let Some(ref mut notifier) = self.notifier {
                            notifier.ok(&errors, &whoami).await;
                        }

                        errors.clear();
                    }
                }
            };

            if !errors.is_empty() {
                if let Some(ref mut notifier) = self.notifier {
                    notifier.notify(&errors, &whoami).await;
                }
            }
        }
    }

    #[tracing::instrument(skip(self))]
    async fn poll(
        self: &Polling,
        _poller_update_tx: PollerUpdateChannel,
    ) -> Option<Vec<HelzCheckStatus>> {
        // Checks if we require a systemd unit to be enabled for the poller to run.
        if self.require_systemd_enabled {
            if let Some(systemd_unit) = &self.systemd_unit {
                let systemd = systemd_lib::Systemd::default();

                let state = systemd.get_state(
                    systemd_unit,
                    &systemd_lib::UnitState::Enabled(systemd_lib::EnabledState::Unknown),
                );

                match state {
                    Ok(systemd_lib::UnitState::Enabled(enabled_state)) => {
                        if enabled_state == systemd_lib::EnabledState::Disabled {
                            debug!(
                                "not running health check because unit '{}' is disabled in systemd",
                                systemd_unit
                            );
                            return None;
                        }
                    }
                    Ok(systemd_lib::UnitState::Active(_)) => (), // we shouldn't get 'activestate' for this check.
                    Err(err) => {
                        error!("failed to get systemd unit enabled state when checking if we should run health check for unit '{}': {:?}", systemd_unit, err);
                    }
                }
            }
        }

        let mut errors = Vec::new();
        for poller in &self.pollers {
            match poller.run().await {
                Ok(data) => {
                    debug!("wooi, datas! {:?}", data);
                }
                Err(err) => {
                    debug!("error res for {}", self.name);
                    errors.push(HelzCheckStatus {
                        name: self.name.to_owned(),
                        response: err.to_owned(),
                        time: time::Instant::now(),
                        human_time: Local::now(),
                    });
                }
            }
        }

        match errors.len() {
            0 => None,
            _ => Some(errors),
        }
    }
}
