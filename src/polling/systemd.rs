/*! Systemd(1) unit poller
 */
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::convert::From;

use systemd_lib::{ActiveState, Systemd as SystemdLib};

use crate::config::traits::Example;
use crate::polling::config::{HelzCheck, HelzCheckError, HelzCheckResponse};

/// Configuration for the systemd(1) unit poller
///
/// The values can be configured in TOML as follows:
/// ```
/// # use libhelz::polling::systemd::SystemdConfig;
/// # let conf: SystemdConfig = toml::from_str(r#"
/// unit = "helz"
/// require_enabled = false  # optional
/// # "#).expect("example configuration should work");
/// ```
///
/// Full example:
/// ```
/// # use libhelz::config::main::configure;
/// # let config = configure(r#"
/// [[polling]]
/// [polling.systemd]
/// unit = "helz"
/// [polling.notifier]
/// interval = "300s"
/// [polling.notifier.stdout]
/// prefix = "tcp"
/// # "#).expect("example configuration should work");
/// ```
#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Eq)]
pub struct SystemdConfig {
    /// Name of systemd(1) unit to check for active state.
    pub unit: SystemdUnitName,
    /// If the unit has to be enabled to be checked. Defaults to false.
    /// Setting this to true disables warnings for disabled services.
    pub require_enabled: Option<bool>,
    /// Path to systemd
    pub path: Option<Path>,
}

/// The runtime struct of the systemd poller.
pub struct Systemd {
    /// Name of systemd(1) unit to check for active state.
    pub unit: SystemdUnitName,
    /// Only report errors if the unit is enabled in systemd(1).
    pub require_enabled: bool,
    // Systemd lib instance
    lib: SystemdLib,
}

type SystemdUnitName = String;
// @ToDo: find out if we have a native file path we can use.
type Path = String;

impl From<SystemdConfig> for Systemd {
    fn from(conf: SystemdConfig) -> Self {
        let mut lib = SystemdLib::default();
        if let Some(path) = conf.path {
            lib = SystemdLib::new(&path);
        }

        Systemd {
            unit: conf.unit,
            require_enabled: conf.require_enabled.unwrap_or(false),
            lib,
        }
    }
}

#[async_trait]
impl HelzCheck for Systemd {
    async fn run(self: &Systemd) -> Result<HelzCheckResponse, HelzCheckError> {
        if self.require_enabled {
            // If configured to only report errors if the unit is 'enabled',
            // we have to check if it is enabled.
            let enabled_state = self
                .check_unit_enabled_state()
                // Default to assuming the unit is enabled.
                .unwrap_or(systemd_lib::EnabledState::Enabled);

            if enabled_state != systemd_lib::EnabledState::Enabled {
                // Here we report an OK state because we were configured to only run for enabled units,
                // we checked whether the unit was enabled or not,
                // and we found it to be **not** enabled, so no matter the result of the check we will not
                // notify. Therefore, we can avoid doing the ActiveState check and simply return OK.
                return Ok(HelzCheckResponse { success: true });
            }
        }

        self.check_unit_active_state()
    }
}

impl Systemd {
    fn check_unit_active_state(self: &Systemd) -> Result<HelzCheckResponse, HelzCheckError> {
        let request_state = systemd_lib::UnitState::Active(ActiveState::Unknown);

        let state = self
            .lib
            .get_state(&self.unit, &request_state)
            .map_err(|_| {
                HelzCheckError::SystemdFailure("failed to get systemd unit state".to_string())
            })?;

        let active_state: systemd_lib::ActiveState =
            if let systemd_lib::UnitState::Active(state) = state {
                state
            } else {
                return Err(HelzCheckError::SystemdFailure(
                    "invalid state type returned by systemd lib".to_string(),
                ));
            };

        match active_state {
            ActiveState::Active => Ok(HelzCheckResponse { success: true }),
            ActiveState::Activating => Err(HelzCheckError::SystemdFailure(format!(
                "unit '{}' is activating",
                self.unit
            ))),
            ActiveState::Deactivating => Err(HelzCheckError::SystemdFailure(format!(
                "unit '{}' is deactivating",
                self.unit
            ))),
            ActiveState::Inactive => Err(HelzCheckError::SystemdFailure(format!(
                "unit '{}' is inactive",
                self.unit
            ))),
            ActiveState::Failed => Err(HelzCheckError::SystemdFailure(format!(
                "unit '{}' is failed",
                self.unit
            ))),
            ActiveState::Unknown => Err(HelzCheckError::SystemdFailure(format!(
                "unit '{}' is unknown",
                self.unit
            ))),
        }
    }

    fn check_unit_enabled_state(
        self: &Systemd,
    ) -> Result<systemd_lib::EnabledState, HelzCheckError> {
        let request_state = systemd_lib::UnitState::Enabled(systemd_lib::EnabledState::Unknown);

        let state = self
            .lib
            .get_state(&self.unit, &request_state)
            .map_err(|_| {
                HelzCheckError::SystemdFailure("failed to get systemd unit state".to_string())
            })?;

        let enabled_state: systemd_lib::EnabledState =
            if let systemd_lib::UnitState::Enabled(state) = state {
                state
            } else {
                return Err(HelzCheckError::SystemdFailure(
                    "invalid state type returned by systemd lib".to_string(),
                ));
            };

        Ok(enabled_state)
    }
}

impl Example for SystemdConfig {
    fn example() -> Self {
        Self {
            unit: "helz.service".to_string(),
            require_enabled: Some(false),
            path: None,
        }
    }
}
