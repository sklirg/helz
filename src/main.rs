/*!
A health checker program.

This is a very simple binary target for running helz,
the documentation for how to use the various
configuration parameters can be found in the modules of
[libhelz](../libhelz).
*/

extern crate clap;
extern crate serde_json;
#[macro_use]
extern crate log;
extern crate env_logger;
extern crate pretty_env_logger;

use clap::{Parser, Subcommand};
use std::path::Path;

use libhelz::config::file::read_config_file;
use libhelz::config::main::Config;
use libhelz::config::traits::Example;
use libhelz::polling::config::{PollerUpdate, Polling};

mod config_watch;
mod helz_tracing;
mod logging;
mod signal_hooks;

use config_watch::{watcher as config_file_watcher, NotifyWatcher};
use helz_tracing::configure_open_telemetry;
use logging::init_logger;
use signal_hooks::{watcher as signals_watcher, SignalEvent};

/// The unsuccessful variants this program can
/// exit with.
#[derive(Debug)]
enum ProgramError {
    ConfigError,
}

#[derive(Parser, Debug)]
#[clap(author, version, about)]
struct Cli {
    /// Path to TOML configuration file
    #[clap(short = 'f', long = "config", default_value = "conf.toml")]
    config: std::path::PathBuf,

    /// Log level to display (and above).
    /// This can be configured more in detail using the
    /// environment variable RUST_LOG, to specify which
    /// *crates* to apply a specific filter to.
    /// In the packaged versions of Helz, logging from the
    /// AWS SDK is significantly reduced by setting
    /// RUST_LOG=aws_smithy_http_tower=warn in the default
    /// unit file.
    #[clap(short, long)]
    loglevel: Option<String>,

    /// Enable export of OpenTelemetry tracing. Configure it by setting
    /// the normal OTel environment variables, such as
    /// OTEL_EXPORTER_OTLP_ENDPOINT=http://localhost:4317
    #[clap(long)]
    enable_tracing: bool,

    /// Watch configuration for changes and reload Helz
    /// if any changes are noticed. Defaults to false.
    #[clap(short, long)]
    watch: bool,

    /// Verify configuration and print it to stdout
    /// Use the 'check' program instead, through
    /// `helz check`.
    #[clap(short, long)]
    #[deprecated]
    check: bool,

    #[clap(subcommand)]
    programs: Option<CliPrograms>,
}

#[derive(Subcommand, Debug)]
#[clap(author, version, about)]
enum CliPrograms {
    /// Show example configuration
    #[clap(alias = "config")]
    ShowConfig,

    /// Check configuration
    #[clap(alias = "check")]
    CheckConfig,
}

struct App {
    // #[allow(dead_code)] to make sure we don't drop the watcher, which
    // stops it from watching.
    #[allow(dead_code)]
    config_watcher: Option<NotifyWatcher>,
}

struct Helz {
    config_path: std::path::PathBuf,
    config: Option<Config>,
    pollers: Option<Vec<Polling>>,
    signals: tokio::sync::watch::Receiver<SignalEvent>,
    config_changed: tokio::sync::watch::Receiver<Option<Config>>,
    pollers_update: tokio::sync::mpsc::UnboundedReceiver<PollerUpdate>,
}

type PollerUpdateChannel = tokio::sync::mpsc::UnboundedSender<PollerUpdate>;

#[tokio::main]
async fn main() -> Result<(), ProgramError> {
    let cli = Cli::parse();

    if cli.enable_tracing {
        configure_open_telemetry();
    } else {
        init_logger(cli.loglevel);
    }

    if let Some(program) = &cli.programs {
        match program {
            CliPrograms::CheckConfig => {
                let config = parse_config(&cli.config)?;
                println!(
                    "{}",
                    toml::to_string(&config)
                        .expect("expecting parsed config to be able to serialize")
                );
                return Ok(());
            }
            CliPrograms::ShowConfig => {
                // generate an example configuration to print to stdout
                let config = libhelz::config::main::Config::example();
                println!(
                    "{}",
                    toml::to_string(&config).expect("expecting example config to work")
                );
                return Ok(());
            }
        }
    }

    #[allow(deprecated)]
    if cli.check {
        let config = parse_config(&cli.config)?;
        warn!("the use of the check flag has been deprecated, use the check program instead");
        println!(
            "{}",
            toml::to_string(&config).expect("expecting parsed config to be able to serialize")
        );
        return Ok(());
    }

    if !cli.config.exists() {
        error!(
            "Configuration file not found: '{}'",
            cli.config
                .to_str()
                .expect("failed to convert supplied string into a path")
        );
        return Err(ProgramError::ConfigError);
    }

    let (tx, rx) = tokio::sync::watch::channel::<SignalEvent>(SignalEvent::Signal(10));
    let signals = signal_hook_tokio::Signals::new([
        signal_hook::consts::signal::SIGHUP,
        signal_hook::consts::signal::SIGINT,
    ])
    .unwrap();
    tokio::spawn(async move {
        signals_watcher(signals, tx).await;
    });

    let (config_tx, config_rx) = tokio::sync::watch::channel::<Option<Config>>(None);
    let mut config_watcher = None;
    if cli.watch {
        let p = Path::new(&cli.config).to_owned();
        config_watcher = match config_file_watcher(&p, config_tx) {
            Ok(w) => Some(w),
            Err(err) => {
                error!(
                    "Failed to configure configuration file watcher: \"{}\", therefore, it is not enabled",
                    err
                );
                None
            }
        }
    }

    let _app = App { config_watcher };

    let (poller_update_tx, poller_update_rx) =
        tokio::sync::mpsc::unbounded_channel::<PollerUpdate>();

    let mut helz = Helz {
        config_path: cli.config.to_owned(),
        config: None,
        pollers: None,
        signals: rx.to_owned(),
        config_changed: config_rx.to_owned(),
        pollers_update: poller_update_rx,
    };

    trace!("confing helz from main");
    helz.configure(poller_update_tx.to_owned())
        .await
        .expect("failed to configure Helz");

    trace!("starting helz from main");
    helz.start(poller_update_tx).await
}

impl Helz {
    #[tracing::instrument(skip(self, poller_update_tx))]
    async fn start(self, poller_update_tx: PollerUpdateChannel) -> Result<(), ProgramError> {
        info!("starting Helz");
        self.run(poller_update_tx).await
    }

    #[tracing::instrument(skip(self, poller_update_tx))]
    async fn run(mut self, poller_update_tx: PollerUpdateChannel) -> Result<(), ProgramError> {
        let name = &self
            .config
            .as_ref()
            .expect("expecting to have a config when starting Helz")
            .name
            .as_ref()
            .expect("expecting name to be set")
            .to_owned();

        let mut signals = self.signals.clone();
        let mut fs_event = self.config_changed.to_owned();
        let mut handlers: std::collections::HashMap<String, tokio::task::JoinHandle<()>> =
            std::collections::HashMap::new();

        loop {
            tokio::select! {
                Some(poller_update) = self.pollers_update.recv() => {
                    debug!("got poller update");

                    let whoami = name.to_owned();

                    let mut poller = match poller_update {
                        PollerUpdate::Add(p) => {
                            debug!("adding");
                            p
                        },
                        PollerUpdate::Configure(p) => {
                            debug!("adding");
                            Polling::from(p)
                        },
                        PollerUpdate::Remove(p) => {
                            debug!("removing");

                            let name = p.name.to_owned();

                            debug!("Removing poller '{}'", name);

                            if let Some(handler) = handlers.remove(&name) {
                                handler.abort();
                            };
                            continue
                        },
                        PollerUpdate::Deconfigure(p) => {
                            debug!("removing");

                            let name = p.name.unwrap().to_owned();

                            debug!("Removing poller '{}'", name);

                            if let Some(handler) = handlers.remove(&name) {
                                handler.abort();
                            };
                            continue
                        },
                    };


                    let name = poller.name.to_owned();

                    // XXX: SUPERHACK
                    // Avoid adding a new poller with the same name as an existing one.
                    if handlers.contains_key(&name) {
                        trace!("Poller map already contains {} so we skip adding it.", name);
                        continue
                    }

                    info!("Starting pollers for '{}'", &name);

                    // We need this handle in scope to avoid the channel being dropped.
                    let dont_drop_this = poller_update_tx.to_owned();

                    match poller.configure_pollers(dont_drop_this.to_owned()).await {
                        Ok(_) => info!("Configured poller {}", name),
                        Err(e) => {
                            error!("Failed to configure Helz: {}", e);
                            return Err(ProgramError::ConfigError);
                        },
                    };

                    let handler = tokio::spawn(async move {
                        let _dont_drop_this = dont_drop_this.to_owned();
                        poller.start(whoami, _dont_drop_this).await;
                    });

                    // Abort existing handler in the map if any.
                    if let Some(handler) = handlers.insert(name, handler) {
                        handler.abort();
                    };
                }


                // XXX: this cancels as soon as 1 returns, I think.. not really what I want
                /*
                _ = handlers.next() => {
                    debug!("pollers stopped");
                    break
                }
                */
                Ok(_s) = signals.changed() => {
                    debug!("got signal");
                    let _signal = *signals.borrow_and_update();

                    if matches!(&_signal, SignalEvent::Stop) {
                        info!("Bye!");
                        return Ok(());
                    }

                    self.configure(poller_update_tx.to_owned()).await.expect("should handle config error on recognition");

                    continue
                }

                Ok(_s) = fs_event.changed() => {
                    debug!("got fsevent");
                    match fs_event.borrow().clone() {
                        Some(new_config) => {
                            if self.config.is_none() {
                                continue;
                            }
                            let current_config = self.config.as_ref().unwrap();
                            if new_config != *current_config {
                                info!("Configuration file changed, reloading Helz");

                                // Allow file changes to sync
                                tokio::time::sleep(std::time::Duration::from_secs(1)).await;

                                // actually handles it fine, just spams logs :)
                                // XXX: is this still relevant
                                self.configure(poller_update_tx.to_owned()).await.expect("should handle config error on recofingure");

                                continue;
                            } else {
                                trace!("config is the same... skipping");
                            }
                        },
                        None => warn!("name equal, not updating"),
                    }
                }
            }
        }
    }

    #[tracing::instrument(skip(self, poller_update_tx))]
    async fn configure(
        &mut self,
        poller_update_tx: PollerUpdateChannel,
    ) -> Result<(), ProgramError> {
        debug!("configuring Helz");

        if self.config.is_some() {
            info!("reloading Helz configuration");
        }

        let config = parse_config(&self.config_path)?;
        self.config = Some(config.to_owned());

        for poller in config.polling {
            trace!("adding poller");
            let mut p = Polling::from(poller.to_owned());
            if let Err(err) = p.configure_pollers(poller_update_tx.to_owned()).await {
                error!("failed to configure Helz: {:?}", err);
                return Err(ProgramError::ConfigError);
            }

            if let Some(existing_pollers) = &mut self.pollers {
                // NOTE: this triggers when adding more than 1 poller.
                // maybe check for poller name before "alerting" about replacing?
                debug!("replacing existing pollers with new ones");
                let update_existing = existing_pollers
                    .iter_mut()
                    .any(|existing| existing.name == p.name);
                if update_existing {
                    trace!("updating existing poller for {}", p.name);
                    for existing in existing_pollers.iter_mut() {
                        if existing.name == p.name {
                            // TODO: consider if we should keep alert state
                            // I think it makes most sense to update
                            // "display" values of an alert rather than the values
                            // actually being checked, which means that values
                            // changed will only change how the alert is displayed
                            // - e.g. the name of it.
                            // If the checking value is changed, it's fair for it to trigger
                            // a new alert in my mind.
                            // The only time I think it makes sense to avoid triggering a new
                            // alert (if one is active) is if the config is exactly the same --
                            // in which case we can abort the configuration upgrade for that
                            // poller in specific.
                            debug!("re-configuring existing poller for {}", p.name);
                            match poller_update_tx
                                .send(PollerUpdate::Deconfigure(poller.to_owned()))
                            {
                                Ok(_) => (),
                                Err(e) => warn!("failed to initiate removal of poller: {}", e),
                            };
                            match poller_update_tx.send(PollerUpdate::Configure(poller.to_owned()))
                            {
                                Ok(_) => (),
                                Err(e) => warn!("failed to re-add existing poller: {}", e),
                            };
                            break;
                        }
                    }
                } else {
                    debug!("adding poller {:p} to helz", &poller);
                    match poller_update_tx.send(PollerUpdate::Configure(poller.to_owned())) {
                        Ok(_) => (),
                        Err(e) => warn!("failed to initiate adding new poller: {}", e),
                    };
                }
            } else {
                if self.pollers.is_none() {
                    self.pollers = Some(vec![]);
                }
                if let Some(_pollers) = &mut self.pollers {
                    debug!("adding poller to helz");
                    match poller_update_tx.send(PollerUpdate::Configure(poller.to_owned())) {
                        Ok(_) => (),
                        Err(e) => warn!("failed to initiate adding new poller: {}", e),
                    };
                }
            }
        }

        debug!("done configuring");

        Ok(())
    }
}

#[tracing::instrument]
fn parse_config(path: &std::path::PathBuf) -> Result<Config, ProgramError> {
    debug!("parsing configuration");

    let config = match read_config_file(path) {
        Ok(conf) => conf,
        Err(err) => {
            error!("failed to configure Helz: {:?}", err);
            return Err(ProgramError::ConfigError);
        }
    };

    Ok(config)
}
