FROM rust:latest AS builder

RUN mkdir /app
WORKDIR /app

# Systemd lib
WORKDIR /app/systemd-lib
COPY Cargo.lock .
COPY Cargo.toml .
COPY systemd-lib/ .
RUN cargo fetch --locked

# Helz
WORKDIR /app
COPY Cargo.lock .
COPY Cargo.toml .
RUN cargo fetch --locked

COPY . .
RUN cargo build --release --locked

# Release image
FROM gcr.io/distroless/cc-debian11

COPY --from=builder /app/target/release/helz /bin/helz

ENTRYPOINT ["/bin/helz"]
