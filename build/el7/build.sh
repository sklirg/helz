#!/bin/sh

set -e

VERSION_NO=${1:-"0.0.0"}
BUILD_NO=${2:-1}

echo "Building Helz RPM v${VERSION_NO} build ${BUILD_NO}"

sleep 3

# clean
cd build/el7/rpmbuild ; make clean ; cd -

# prep
make build/el7/NEWS

# rpm-prep
cd build/el7

make rpmbuild/SPECS/helz.spec VERSION_NO=${CARGO_VERSION} BUILD_NO=${BUILD_NO:-1}

# build
docker build -f build/el7/Dockerfile . --build-arg HELZ_VERSION=${VERSION_NO} --build-arg BUILD_NO=${BUILD_NO} 2>&1 | tee output.txt

exit 0
docker build -f build/el7/Dockerfile . --build-arg HELZ_VERSION=${VERSION_NO} --build-arg BUILD_NO=${BUILD_NO} 2>&1 | tee output.txt
IMAGE_SHA=$(grep "Successfully built" output.txt | sed 's/.*Successfully built \([0-9a-zA-Z]*\).*/\1/')
docker run --rm --name helz-export-${IMAGE_SHA} -d -t ${IMAGE_SHA}
docker cp helz-export-${IMAGE_SHA}:/release/${EL7_AMD64_RPM} .
docker stop helz-export-${IMAGE_SHA}
